import styled from 'styled-components';
// import { TextField } from './components/TextField/';


// const StyledTextField = styled(TextField)`
//   border-width: 2px;
//   border-style: dashed;
//   border-color: #1166ff;
//   box-shadow: 0 4px 4px #1166ff;
//   outline: none;
// `
const FormRow = styled.form`
  width: 500px;
  margin: 20px auto;
  // border-style: solid
`

const StyledInput = styled.input`
width : 200px;
margin-bottom: 40px;`


// const StyledNavbar = styled.navbar``
const SideLink = styled.link`
margin-right: 20px;
color: #fff;
text-decoration: none;
margin-bottom: 25px;
`

const SideNavbar = styled.link`
margin-right: 20px;
color: #fff;
text-decoration: none;
margin-bottom: 25px;
`

const Navbar = styled.nav`
z-index: 5;
position: fixed;
width: 100%;
top: 0px;
left: 0px;
border-style: dashed;
`
export {
  // StyledTextField,
  FormRow,
  StyledInput,
  SideLink,
  Navbar
};